package com.maem.maem.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Controller
public class MaemController {

    @GetMapping("/")
    public String home(@RequestParam(name="name", required=false) String name, Model model) {
        model.addAttribute("name", name);
        return "home";
    }

    @GetMapping("/restaurant")
    public String restaurant(@RequestParam(name="name", required = false) String name, Model model) {
        model.addAttribute("name", name);
        return "restaurant";
    }

    @GetMapping("/voucher")
    public String voucher(@RequestParam(name="name", required = false) String name, Model model) {
        model.addAttribute("name", name);
        return "voucher";
    }

    @GetMapping("/order")
    public String order(@RequestParam(name="name", required = false) String name, Model model) {
        model.addAttribute("name", name);
        return "order";
    }

    @GetMapping("/menu")
    public String menu(@RequestParam(name="name", required = false) String name, Model model) {
        model.addAttribute("name", name);
        return "menu";
    }
}